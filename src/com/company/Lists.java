package com.company;

import java.util.ArrayList;
import java.util.LinkedList;

public class Lists {
    public static void Display(){
        LinkedList<String> ll = new LinkedList<>();
        ll.add("Linked");
        ll.add("List");
        ArrayList<String> al = new ArrayList<>();
        al.add("dummy");
        al.add("list");
        al.add("of type ArrayList");
        System.out.println("Linked list: " + ll);
        System.out.println("Array List: " + al);
    }
}
