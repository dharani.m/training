package com.company;

import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

public class HashSets {
    public static void Main() {

        HashSet<Integer> hashSet = new HashSet<>();
        hashSet.add(450);
        hashSet.add(350);
        hashSet.add(900);
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(100);
        treeSet.add(800);
        treeSet.add(400);
   System.out.println("Tree Set: " + treeSet);
   System.out.println("Hash Set : ");
        for (Integer temp : hashSet) {
            System.out.println(temp);
        }
        HashMap<String, String> hm = new HashMap<>();
        hm.put("firstName","John");
        hm.put("lastName","Doe");
        hm.put("email","johndoe@point.com");
        System.out.println("Hash Map :" + hm);
        System.out.println(hm.get("email"));

    }

}
