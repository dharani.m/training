package com.company;

public class Coordinates {
    int x,y;
    public Coordinates(int x, int y){
        this.x = x;
        this.y = y;
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public final boolean equals(Object obj){
        Coordinates checkObj = (Coordinates) obj;
        return this.x == checkObj.x && this.y == checkObj.y;
    }

}
