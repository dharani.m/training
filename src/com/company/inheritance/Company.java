package com.company.inheritance;

public class Company {
    public String name;
    public String address;

    public void setCompanyDetails(String name, String address){
        this.name = name;
        this.address = address;
    }
    public String getCompanyName(){
        return this.name;
    }

    public String getCompanyAddress(){
        return this.address;
    }

    public void displayCompanyDetails(){
        System.out.printf("Company Name: %s\nCompany Address: %s",this.name,this.address);
    }

}
