package com.company.inheritance;

public class Employee {
    public int id;
    public String name;
    public String designation;

    public Employee(int id,String name,String designation){
        this.id = id;
        this.name = name;
        this.designation = designation;
    }

    public void displayEmployeeDetails(){
        System.out.printf("Employee id : %d\nEmployee Name : %s\nEmployee Designation : %s",this.id,this.name,this.designation);
    }
}
