package com.company.inheritance;

public class Project extends Company{
    private String name;
    public Employee lead;

    public void setName(String name){
        this.name = name;
    }

    public void setLead(Employee lead){
        this.lead = lead;
    }
    public String getName(){
        return this.name;
    }
    public void displayProjectDetails(){
        System.out.println("Project Name: " + this.name);
        displayCompanyDetails();
        System.out.println("Assigned to :");
        lead.displayEmployeeDetails();
    }
}
