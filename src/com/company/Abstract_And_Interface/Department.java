package com.company.Abstract_And_Interface;

public abstract class Department{
    public int count = 0;
    public int getCount(){
        return this.count;
    }
    public abstract void addStaff(String name);
    public abstract void displayStaffDetails();
}
