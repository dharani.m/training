package com.company.Abstract_And_Interface;

public interface Salary {
    void setDepartmentSalary(int salary);
    int getDepartmentSalary();
}
