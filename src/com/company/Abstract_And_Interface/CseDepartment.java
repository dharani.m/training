package com.company.Abstract_And_Interface;

import java.util.ArrayList;

public class CseDepartment extends Department implements Salary{
    public ArrayList<String> staffList = new ArrayList<>();
    public int deptSalary;
    @Override
    public void addStaff(String staffName) {
            staffList.add(staffName);
            this.count++;
    }
    @Override
    public void displayStaffDetails(){
        System.out.println(staffList);
    }

    @Override
    public void setDepartmentSalary(int salary) {
        this.deptSalary = salary;
    }

    @Override
    public int getDepartmentSalary() {
        return this.deptSalary;
    }
}
