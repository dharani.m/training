package com.company;

public class CompareObjects {
    public static void main(){
        Coordinates coord1 = new Coordinates(10,25);
        Coordinates coord2 = new Coordinates(10,25);
        System.out.println("Checking with .equals() : " + coord1.equals(coord2));
        System.out.println("Checking with .hashCode() : " + coord1.hashCode() + " " + coord2.hashCode());

    }
}
