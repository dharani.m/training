package com.company;

import com.company.Abstract_And_Interface.CseDepartment;
import com.company.Abstract_And_Interface.Department;
import com.company.inheritance.Employee;
import com.company.inheritance.Project;
import com.company.polymorphism.Car;
import com.company.polymorphism.PetrolCar;
import com.company.polymorphism.Tiago;

import java.io.PrintStream;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        //Filtering data using Streams
        filterFirst(new String[] {"apple","Ball","cat","dog","Example"},"a",3);
        filterLast(new String[] {"apple","Ball","cat","dog","Example"},"l",3);
        filter(new String[] {"apple","Ball","cat","dog","Example"},"le");



        //Polymorphism. Method overloading and overriding
        Car tiagoXM = new Tiago();
        tiagoXM.mileage();
        Tiago tiagoXZ = new Tiago();
        tiagoXZ.mileage();
        PetrolCar tiagoXT = new Tiago();
        tiagoXT.mileage();
        PetrolCar someCar = new PetrolCar();
        someCar.mileage();

        Tiago tiagoXZA = new Tiago("John");
        tiagoXZA.displayOwner();

        //2D Arrays
        int[][] a = {{25,36,78,63},{98,778,45}};
        System.out.println(a[1][2]);

        //ArrayList Implementation using Generics
        GenericArrayList<String> gal = new GenericArrayList<>();
        gal.add("Str1");
        gal.add("Str2");
        gal.add("Str3");
        gal.add("Str4");
        System.out.println(gal.get(3));

        //String Manipulation
        String str = "This is a sample string";
        String strToSearch = "str";
        System.out.println("String contains \"str\" : "+ str.contains(strToSearch));
        System.out.println(str.substring(10,16));
        char[] charArr = str.toCharArray();
        StringBuilder newStr = new StringBuilder();
        newStr.append(str);
        System.out.println(newStr.reverse());

        //Date and time
        LocalDate currDate = LocalDate.now();
        System.out.println("Current time plus five days : " + currDate.plusDays(4));

        Date currentTime = new Date();
        System.out.println("Current time : " + currentTime);
        currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        System.out.println(calendar.getTimeZone());


        //lists, hashsets, equals(), hashcode()
        Lists.Display();
        HashSets.Main();
        CompareObjects.main();

        //Abstract classes and Interfaces
        CseDepartment csDept = new CseDepartment();
        csDept.addStaff("xxx");
        csDept.addStaff("yyy");
        System.out.println("Cse department has : " + csDept.count + " members.");
        System.out.println("Staffs List of Cse department is ");
        csDept.displayStaffDetails();
        csDept.setDepartmentSalary(35000);
        System.out.println("Department Salary : " + csDept.getDepartmentSalary());
        System.out.println();

        //Inheritance
        Project tinyMe = new Project();
        tinyMe.setCompanyDetails("ABC Tech", "Moolapalayam, Erode-638002");
        tinyMe.setLead(new Employee(78,"XYZ","SE2"));
        tinyMe.displayProjectDetails();

    }

    //Filtering Data using Streams
    public static void filterFirst(String[] s,String query, int n){
        System.out.println("Unfiltered : "+ Arrays.toString(s) + "\nFiltered: with query: \""+ query+ "\" and n="+n);
        Arrays.stream(s).filter(x->x.contains(query)).limit(n).forEach(System.out::println);
    }
    public static void filterLast(String[] s,String query,int n){
        System.out.println("Unfiltered : "+ Arrays.toString(s) + "\nFiltered: with query: \""+ query+ "\" and n="+n);
        Arrays.stream(s).skip(s.length-n).filter(x->x.contains(query)).forEach(System.out::println);
    }
    public static void filter(String[] s,String query){
        System.out.println("Unfiltered : "+ Arrays.toString(s) + "\nFiltered: with query: \""+ query+ "\"");
        Arrays.stream(s).filter(x->x.contains(query)).forEach(System.out::println);
    }
}
