package com.company;

import java.util.Arrays;

public class GenericArrayList<T> {
    private int i = 0;
    private Object[] obj;
    private T t;

    public GenericArrayList(){
        obj = new Object[3];
    }
    public void add(T t){
        this.t = t;
        if(i==obj.length-1){
            obj = Arrays.copyOf(obj,obj.length + (obj.length/2));
        }
        obj[i] = this.t;
        i++;
    }
    public Object get(int i){
        if(obj[i] != null){
            return obj[i];
        } else{
            throw new ArrayIndexOutOfBoundsException();
        }
    }
}
