package com.company.polymorphism;

public class PetrolCar extends Car{
    public void mileage(){
        System.out.println("Average mileage of a petrol car is between 10 and 25");
    }
}
