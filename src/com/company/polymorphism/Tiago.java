package com.company.polymorphism;

public class Tiago extends PetrolCar{
    public String make = "Tata";
    public static int count = 0;
    public String owner;
    public Tiago(){
        count++;
    }
    public Tiago(String name){
        this.owner = name;
        count++;
    }
    public void mileage(){
        System.out.println("Mileage of " + this.make + " Tiago is 23");
    }
    public void displayOwner(){
        if(this.owner == null){
            System.out.println("Owner name hasn't been registered yet!");
        } else{
            System.out.println("Owner of this car is " + this.owner);
        }
    }
}
